# recipe-app-api-proxy
# Recipe app API proxy application

NGIBIX  proxy app for our recipe app API

## Usage
### Environnement Variables


* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of yjr app to  forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forword requests to (default: `9000`)